#!/usr/bin/env python3

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#    AUTHOR: Edelmiro Moman 2020, moman@prosciens.com
#    https://sciting.eu

####################
#       DREPY      #
####################

# Minimalist structure-based Drug REpositioning workflow in PYthon.

# DREPY is a minimalist Python workflow that aims at matching the structure 
# of approved drugs to that of potential target proteins.

# The current version targets proteins of the SARS-CoV-2 virus which caused the COVID-19 pandemic.
# However, it can be very easily modified to target other organisms and/or protein families
# or, for instance, to also include experimental drugs or any other subset.

# For installation and usage instructions see INSTALL.txt

# For further information refer to:
# https://sciting.eu/sciting/samples/covid-19-repurposing/

####################
#     LIGANDS      #
####################

# We will use CHEMBL Python API
# which relies on Django
# in order to query CHEMBL for molecules

from chembl_webresource_client.new_client import new_client
molecule = new_client.molecule

# We search CHEMBL for approved (Phase IV) small molecules drugs
# If you wish to include experimental drugs, you can change "max_phase=4" to 3
# or use different search parameters

approved_drugs = molecule.filter(max_phase=4, molecule_type='Small molecule').only(['molecule_chembl_id', 'pref_name', 'molecule_structures']).all()

import pandas as pd

from pathlib import Path

# We check if a previosly created CSV file exists
# If it exists, we import it as a Pandas dataframe
# If it doesn't, we create an empty dataframe

import os
if not os.path.exists('LIGANDS'):
	os.makedirs('LIGANDS')

my_file = Path('CHEMBL_APPROVED_DRUGS.csv')
if my_file.is_file():
	df_old = pd.read_csv('CHEMBL_APPROVED_DRUGS.csv')
else:
	df_old = pd.DataFrame(columns=['CHEMBL_ID', 'NAME', 'SMILES'])

df_new = pd.DataFrame(columns=['CHEMBL_ID', 'NAME', 'SMILES'])

# We create a new dataframe (df) which contains only the new drugs
# We also append those new drugs (if applicable) to the existing dataframe (df_old)
# If this is the first time we are running this script all drugs are new
# If, conversely, no new drugs have been added to CHEMBL since the last time we run this script, nothing changes
# For simplicity, drugs without a SMILES structure in CHEMBL are excluded (that is the result of the dict condition below)

from openbabel import openbabel, pybel

import subprocess
ncpu = str(len(os.sched_getaffinity(0)))

for drug in approved_drugs:
	did= drug['molecule_chembl_id']
	struct = drug['molecule_structures']
	if did not in df_old.values and type(struct) is dict:
		dname = drug['pref_name']
		smile = struct['canonical_smiles']
		
		obConversion = openbabel.OBConversion()
		obConversion.SetInAndOutFormats('smi', 'smi')				
		mol = openbabel.OBMol()
		
		obConversion.ReadString(mol, smile)
		frags = mol.Separate()
		maxFrag = None
		maxNAtom = -9999
		for frag in frags:
			if frag.NumAtoms() > maxNAtom:
				maxNAtom = frag.NumAtoms()
				maxFrag = openbabel.OBMol(frag)
		
		outsmi = obConversion.WriteString(maxFrag).replace('\n', '')

		new_drug = {'CHEMBL_ID':did, 'NAME':dname, 'SMILES':outsmi}
		
		if outsmi not in df_new.values and outsmi not in df_old.values:
				
			df_new = df_new.append(new_drug, ignore_index=True)
			
			smip = 'LIGANDS/' + did + '.smi'
			smi = open(smip, 'w' )
			smi.write(smile + ' ' + did)
			smi.close()
			# We convert SMILES to 3D libaries of conformers/tautomers (Gypsum DL)
			try:
#				subprocess.run(['mpirun', '-n', ncpu, 'python', '-m', 'mpi4py',\
#								'gypsum_dl/run_gypsum_dl.py', '--source', smip, '--output_folder', 'LIGANDS', '--separate_output_files',\
#								'--job_manager', 'mpi', '--num_processors', '-1'],\
#								stderr=None, timeout=333)
				subprocess.run(['python', 'gypsum_dl/run_gypsum_dl.py', '--source', smip, '--output_folder', 'LIGANDS', '--separate_output_files', '--job_manager', 'multiprocessing', '--num_processors', ncpu], stderr=None, timeout=333)
				sdf = 'LIGANDS/' + did + '__input1.sdf'
				os.rename(sdf, sdf.replace('__input1', ''))
				sdf = 'LIGANDS/' + did + '.sdf'
				os.remove(smip)
				# We convert ligand conformers to individual PDBQT files (OpenBabel Python API)
				# Only moleculer "larger" than acetic acid will be considered
				if os.path.exists(sdf) and os.path.getsize(sdf) > 886:
					if not os.path.exists('LIGANDS/' + did):
						os.makedirs('LIGANDS/' + did)
					i = 1
					for mol in pybel.readfile('sdf', sdf):
						conf = 'LIGANDS/' + did + '/' + did + '_' + str(i).zfill(2) + '.sdf'
						mol.write('sdf', conf)
					
						obConversion = openbabel.OBConversion()
						obConversion.SetInAndOutFormats('sdf', 'pdbqt')
						
						mol = openbabel.OBMol()
						obConversion.ReadFile(mol, conf) 
						pdbqt = 'LIGANDS/' + did + '/' + did + '_' + str(i).zfill(2) + '.pdbqt'
						obConversion.WriteFile(mol, pdbqt)
						os.remove(conf)
						i += 1
			except Exception:
				pass

# We save the comprehensive dataframe (old + new)
# overwriting the previous CSV file

df_all = pd.concat([df_old, df_new])

df_all.to_csv('CHEMBL_APPROVED_DRUGS.csv', sep=',', index=False)


####################
#     PROTEINS     #
####################

# We query the Protein Data Bank (PDB)
# in order to find SARS-CoV-2 structures 
# For the time being, we will consider exclusively X-ray structures

import requests
import io

url = 'http://www.rcsb.org/pdb/rest/search'

# You can adapt this sample query to different organisms and/or protein families
# or, in general, any other search terms supported by the PDB (sparsely documented, unfortunately)
	
query_text = """
<?xml version="1.0" encoding="UTF-8"?>

<orgPdbCompositeQuery version="1.0">
 <queryRefinement>
  <queryRefinementLevel>0</queryRefinementLevel>
  <orgPdbQuery>
    <version>head</version>
    <queryType>org.pdb.query.simple.OrganismQuery</queryType>
    <description>Organism Search: Organism Name=Severe acute respiratory syndrome coronavirus 2 </description>
    <organismName>Severe acute respiratory syndrome coronavirus 2</organismName>
  </orgPdbQuery>
 </queryRefinement>
   <queryRefinement>
    <queryRefinementLevel>1</queryRefinementLevel>
    <conjunctionType>and</conjunctionType>
    <orgPdbQuery>
     <version>head</version>
     <queryType>org.pdb.query.simple.ResolutionQuery</queryType>
     <description>Resolution is between 0.0 and 4.0 </description>
     <refine.ls_d_res_high.comparator>between</refine.ls_d_res_high.comparator>
     <refine.ls_d_res_high.min>0.0</refine.ls_d_res_high.min>
     <refine.ls_d_res_high.max>4.0</refine.ls_d_res_high.max>
    </orgPdbQuery>
 </queryRefinement>
</orgPdbCompositeQuery>

"""
header = {'Content-Type': 'application/x-www-form-urlencoded'}
response = requests.post(url, data=query_text, headers=header)

# We check if a previosly created CSV file exists
# If it exists, we import it as a Pandas dataframe
# If it doesn't, we create an empty dataframe

pcolumns = ['structureId', 'chainId', 'releaseDate', 'resolution', 'macromoleculeType', 'uniprotAcc', 'authorAssignedEntityName', 'synonym', 'pfamAccession', 'pfamDescription', 'taxonomy']
upcolumns = 'structureId,chainId,releaseDate,Resolution,macromoleculeType,uniprotAcc,authorAssignedEntityName,synonym,pfamAccession,pfamDescription,taxonomy'

my_file = Path('PDB_SARSCOV2_PROTEINS.csv')
if my_file.is_file():
	dfp_old = pd.read_csv('PDB_SARSCOV2_PROTEINS.csv')
else:
	dfp_old = pd.DataFrame(columns = pcolumns)
	
dfp_new = pd.DataFrame(columns = pcolumns)
	
# We create a new dataframe (dfp) which contains only the new protein structures
# We also append those new proteins (if applicable) to the existing dataframe (dfp_old)
# If this is the first time we are running this script all proteins are new
# If, conversely, no new protein structures have been added to the PDB since the last time we run this script, nothing changes

for pdbid in response.text.splitlines():
	if pdbid not in dfp_old.values:
		url2 = 'http://www.rcsb.org/pdb/rest/customReport.csv?pdbids=' + pdbid + '&customReportColumns=' + upcolumns + '&format=csv&service=wsfile'
		response2 = requests.post(url2, headers=header)
		dfp_tmp = pd.read_csv(io.StringIO(response2.text))
		dfp_tmp = dfp_tmp.drop(dfp_tmp[(dfp_tmp['macromoleculeType'] != 'Protein') | (dfp_tmp['taxonomy'] != 'Severe acute respiratory syndrome-related coronavirus') | (dfp_tmp['uniprotAcc'].astype(str).str.contains('#'))].index)
		dfp_new = pd.concat([dfp_new, dfp_tmp])	

# We save the comprehensive dataframe (old + new)
# overwriting the previous CSV file

dfp_all = pd.concat([dfp_old, dfp_new])

dfp_all.to_csv("PDB_SARSCOV2_PROTEINS.csv", sep=',', index=False)

### PDB DOWNLOAD ###
# We creare one directory per gene product (full protein)
# and download the PDB structures to the corresponding directory
# Note that the expressed polypeptide may correspond only to one specific domain
# which means that there may be different polypeptides within the same directory
# as long as they are encoded by the same gene
# This limitation is due to the fact that some of the crystallised structures
# lack domain information (like Pfam) and therefore it is not trivial to classify them
# We intend to address this caveat in new versions of the workflow

import wget
from prody import *
from pdbfixer import PDBFixer
from simtk.openmm.app import PDBFile

if not os.path.exists('PDBS'):
	os.makedirs('PDBS')

#genes = dfp_all.sort_values('resolution').groupby(['uniprotAcc'], sort=False).head(10)
#genes = dfp_new.groupby('uniprotAcc')

dfp_top = pd.DataFrame(columns = pcolumns)
genes = dfp_all.sort_values('resolution').groupby(['uniprotAcc'], sort=False)

# We are going to use only the 3 structures with best resolution per gene product
# If you have enough time and computational resources you may want to increase it to, say, 10
# In order to do so, just change the number below ".head(3)"

for group_name, df_group in genes:
	top = df_group.head(3)
	dfp_top = pd.concat([dfp_top, top])

genes = dfp_top.sort_values('resolution').groupby(['uniprotAcc'], sort=False)

for group_name, df_group in genes:
	if not os.path.exists('PDBS/' + group_name):
		os.makedirs('PDBS/' + group_name)

	for row_index, row in df_group.iterrows():
		pdbid = row['structureId']
		purl = 'https://files.rcsb.org/download/' + pdbid + '.pdb'
		ppath = 'PDBS/' + group_name + '/' + pdbid + '.pdb'
		if not os.path.exists(ppath):
			response = requests.get(purl)
			if response.status_code == 200:
				wget.download(purl, ppath, bar=None)
				
# PDB PREPARATION

for group_name, df_group in genes:
	for row_index, row in df_group.iterrows():
		pdbid = row['structureId']
		chainid = row['chainId']
		ppath = 'PDBS/' + group_name + '/' + pdbid + '.pdb'
		ppatho = 'PDBS/' + group_name + '/' + pdbid + '_' + chainid + '.pdb'
		if os.path.exists(ppath) and not os.path.exists(ppatho):	
			
			# PRODY
			# We split the SARS-CoV-2 by chain
			# We remove alternate locations and ANISOU records
			
			onechain = parsePDB(ppath, chain=chainid)
			writePDB(ppatho, onechain)
			
			### PDB CLEAN-UP ###
			# PDBFIXER
			# We switch non-standard residues to their naturally-occuring homologue
			# We add missing residues and atoms
			# We remove heteroatoms (non-amino acids)
			# We add hydrogen atoms for pH 7
			# We renumber the atoms and residues consecutively (while preserving chain IDs)
			
			fixer = PDBFixer(filename=ppatho)
			fixer.findMissingResidues()
			fixer.findNonstandardResidues()
			fixer.replaceNonstandardResidues()
			fixer.removeHeterogens(False)
			fixer.findMissingAtoms()
			fixer.addMissingAtoms()
			fixer.addMissingHydrogens(7.0)
			PDBFile.writeFile(fixer.topology, fixer.positions, open(ppatho, 'w'), keepIds=True)
			
			# We convert the PDB file to PDBQT for docking (OpenBabel Python API)
			
			obConversion = openbabel.OBConversion()
			obConversion.SetInAndOutFormats('pdb', 'pdbqt')
			obConversion.AddOption('r')				
			mol = openbabel.OBMol()
			obConversion.ReadFile(mol, ppatho) 
			obConversion.WriteFile(mol, 'PDBS/' + group_name + '/' + pdbid + '_' + chainid + '.pdbqt')


####################
#     DOCKING      #
####################

pcolumns = ['CHEMBL_ID', 'ligand_path', 'structureId', 'protein_path', 'deltaG']

my_file = Path('DELTAG_SARSCOV2.csv')
if my_file.is_file():
	docking = pd.read_csv('DELTAG_SARSCOV2.csv')
else:
	docking = pd.DataFrame(columns = pcolumns)

# We compute the binding energy of the new ligands
# towards all protein structures (old and new)

if not os.path.exists('TEMP'):
	os.makedirs('TEMP')

import re
from pathlib import Path
from biopandas.pdb import PandasPdb

deltaG = 10.0
for ligand in Path('LIGANDS').rglob('*.pdbqt'):
	chemblid = Path(ligand).stem.split('_')[0]
	conformer = Path(ligand).stem
	if chemblid in df_new.values and os.path.getsize(ligand) > 0:
		for protein in Path('PDBS').rglob('*.pdbqt'):
			pdbchain = Path(protein).stem
			pdbid = Path(protein).stem.split('_')[0]
			pdbp = str(protein)[:-2]
			
			# We compute the centre and of a box containing our protein
			# with a 6 angstrom padding to accomodate surface-binding ligands 
			
			ppdb = PandasPdb()
			ppdb.read_pdb(pdbp)
			
			x_min = ppdb.df['ATOM']['x_coord'].min()
			x_max = ppdb.df['ATOM']['x_coord'].max()
			x_centre = str(round(( x_max + x_min ) / 2, 2))
			x_box = str(round(x_max - x_min + 6))
			
			y_min = ppdb.df['ATOM']['y_coord'].min()
			y_max = ppdb.df['ATOM']['y_coord'].max()
			y_centre = str(round(( y_max + y_min ) / 2, 2))
			y_box = str(round(y_max - y_min + 6))
			
			z_min = ppdb.df['ATOM']['z_coord'].min()
			z_max = ppdb.df['ATOM']['z_coord'].max()
			z_centre = str(round(( z_max + z_min ) / 2, 2))
			z_box = str(round(z_max - z_min + 6))
			
			# We compute the binding energy (QVina-W) and save it to a dataframe
			# There is a trade-off between computing time and accuracy/reproducibility
			# If the calculations take too long reduce the exhaustiveness
			# If the results are inaccurate/irreproducible, increase the exhaustiveness
			# '--exhaustiveness', '8' (8 times the number of cores)
			
			outpdbqt = 'TEMP/' + pdbchain + '_' + conformer + '.pdbqt'
			try:
				subprocess.run(['qvina/bin/qvina-w', '--receptor', protein, '--ligand', ligand, '--out', outpdbqt, '--num_modes', '1',\
				                '--center_x', x_centre, '--center_y', y_centre, '--center_z', z_centre, '--size_x', x_box, '--size_y', y_box, '--size_z', z_box,\
                                '--cpu', ncpu],\
                                stderr=None, timeout=333)
				with open(outpdbqt) as opdbqt:
					for line in opdbqt:
						if line.startswith('REMARK VINA RESULT:'):
							deltaG = float(re.search(r'[+-]?\d+\.\d+', line).group())
				new_binding = {'CHEMBL_ID':chemblid, 'ligand_path':ligand, 'structureId':pdbid, 'protein_path':pdbp, 'deltaG':deltaG}
				docking = docking.append(new_binding, ignore_index=True)
			except Exception:
				pass

# We compute the binding energy of the new protein structures
# towards the old ligands (the new ones were already considered in the previous step)
deltaG = 10.0
for ligand in Path('LIGANDS').rglob('*.pdbqt'):
	chemblid = Path(ligand).stem.split('_')[0]
	conformer = Path(ligand).stem
	if chemblid in df_old.values and os.path.getsize(ligand) > 0:
		for protein in Path('PDBS').rglob('*.pdbqt'):
			pdbid = Path(protein).stem.split('_')[0]
			if pdbid in dfp_top.values and pdbid not in dfp_old.values:
				pdbchain = Path(protein).stem
				pdbid = Path(protein).stem.split('_')[0]
				pdbp = str(protein)[:-2]
				
				# We compute the centre and of a box containing our protein
				# with a 6 angstrom padding to accomodate surface-binding ligands 
				
				ppdb = PandasPdb()
				ppdb.read_pdb(pdbp)
				
				x_min = ppdb.df['ATOM']['x_coord'].min()
				x_max = ppdb.df['ATOM']['x_coord'].max()
				x_centre = str(round(( x_max + x_min ) / 2, 2))
				x_box = str(round(x_max - x_min + 6))
				
				y_min = ppdb.df['ATOM']['y_coord'].min()
				y_max = ppdb.df['ATOM']['y_coord'].max()
				y_centre = str(round(( y_max + y_min ) / 2, 2))
				y_box = str(round(y_max - y_min + 6))
				
				z_min = ppdb.df['ATOM']['z_coord'].min()
				z_max = ppdb.df['ATOM']['z_coord'].max()
				z_centre = str(round(( z_max + z_min ) / 2, 2))
				z_box = str(round(z_max - z_min + 6))
				
				# We compute the binding energy (QVina-W) and save it to a dataframe
				# There is a trade-off between computing time and accuracy/reproducibility
				# If the calculations take too long reduce the exhaustiveness
				# If the results are inaccurate/irreproducible, increase the exhaustiveness
				# '--exhaustiveness', '8' (8 times the number of cores)
				
				outpdbqt = 'TEMP/' + pdbchain + '_' + conformer + '.pdbqt'
				try:
					subprocess.run(['qvina/bin/qvina-w', '--receptor', protein, '--ligand', ligand, '--out', outpdbqt, '--num_modes', '1',\
					                '--center_x', x_centre, '--center_y', y_centre, '--center_z', z_centre, '--size_x', x_box, '--size_y', y_box, '--size_z', z_box,\
	                                '--cpu', ncpu],\
	                                stderr=None, timeout=333)
					with open(outpdbqt) as opdbqt:
						for line in opdbqt:
							if line.startswith('REMARK VINA RESULT:'):
								deltaG = float(re.search(r'[+-]?\d+\.\d+', line).group())
					new_binding = {'CHEMBL_ID':chemblid, 'ligand_path':outpdbqt, 'structureId':pdbid, 'protein_path':pdbp, 'deltaG':deltaG}
					docking = docking.append(new_binding, ignore_index=True)				
				except Exception:
					pass
					
# We save the new protein-ligand comlexes to the CSV file 

docking.to_csv("DELTAG_SARSCOV2.csv", sep=',', index=False)


####################
#     RESULTS      #
####################

# Only the 10 most promising complexes per gene product are reported

if not os.path.exists('RESULTS'):
	os.makedirs('RESULTS')

results = docking.merge(df_all, on=['CHEMBL_ID'], how='left')
results = results.merge(dfp_all, on=['structureId'], how='left')

results = results[['deltaG', 'CHEMBL_ID', 'NAME', 'structureId', 'uniprotAcc', 'authorAssignedEntityName', 'ligand_path', 'protein_path']]

results.to_csv("RESULTS.csv", sep=',', index=False)

df_res = pd.read_csv('RESULTS.csv')

# If you wish to have more than 10 complexes per gene product, increase the number at the end of this line ".head(10)"

topres = df_res.sort_values('deltaG').drop_duplicates(subset=['uniprotAcc', 'CHEMBL_ID']).groupby(['uniprotAcc'], sort=False).head(10)

topres.to_csv("TOPRES.csv", sep=',', index=False)

images = ['deltaG','CHEMBL_ID', 'NAME', 'structureId', 'uniprotAcc', 'authorAssignedEntityName', 'ligand_path', 'protein_path', 'ligand_structure']
topres = topres.reindex(columns = images)   

from pathlib import Path

# Function convert image paths to HTML tags 
def path_to_image_html(path):
    return '<img src="'+ path + '" width="240" >'
    
# We save the docked ligands in PDB format to the RESULTS folder
# For convenience, a PNG image of the 2D structure is also produced

# If you are wondering why we are saving the PDB files twice,
# well, there seems to be a bug on our version of Pybel and it refuses to add hydrogens...
# This is the simplest workaround we found

for index, row in topres.iterrows():
	ligpath = row['ligand_path']
	basen = Path(ligpath).stem
	
	mol = next(pybel.readfile('pdbqt', ligpath))
	mol.addh()
	pdb = 'RESULTS/' + basen + '.pdb'
	mol.write('pdb', pdb, overwrite=True)
	
	mol2 = next(pybel.readfile('pdb', pdb))
	mol2.addh()
	mol2.write('pdb', pdb, overwrite=True)
	png = os.path.abspath(os.getcwd()) + '/RESULTS/' + basen + '.png'
	mol2.draw(show=False, filename=png, update=False, usecoords=False)
	
	topres.loc[index, 'ligand_path'] = pdb
	topres.loc[index, 'ligand_structure'] = png

# We save the results as CSV and HTML files within the RESULTS folder

topres.to_csv('RESULTS/RESULTS.csv', sep=',', index=False)

pd.set_option('display.max_colwidth', None)
topres.to_html('RESULTS/RESULTS.html', escape=False, index=False, formatters=dict(ligand_structure=path_to_image_html))
