# DREPY

**Minimalist structure-based Drug REpositioning workflow in PYthon.**

ABOUT

**Drug repurposing** (also called drug repositioning, reprofiling or re-tasking) is a strategy for identifying new uses for approved or investigational drugs that are outside the scope of the original medical indication.
 
DREPY is a minimalist Python workflow that aims at matching the structure of approved drugs to that of potential target proteins.

The current version targets proteins of the **SARS-CoV-2** virus which caused the **COVID-19** pandemic.

However, it can be very easily modified to target other organisms and/or protein families.

The advantage of a structure-based approach is that it facilitates lead optimisation as well as de novo design, it can guide cocrystallisation and help identifying druggable pockets.

In addition to its scientific applications, DREPY is conceived as a pedagogical tool.

DREPY has a number of dependencies including QVina-W, OpenBabel (Python bindings), RDKit, BioPandas, Gypsum-DL, and PDBFixer. 

The easiest way of satisfying these dependencies is by installing them within a Conda environment via the conda or pip package managers. For details have a look to the INSTALL file.

WORKFLOW

1. Make a database of approved drugs from CHEMBL.

    -> Generate a collection of conformers/tautomers from each compound. 

2. Download PDB files from potential target proteins.

    -> Fix and clean-up the PDB structures

3. Blind docking of each conformer/tautomer in every protein chain.

4. Make a database of the most interesting drugs for each gene product.


FEATURES

1. Minimalist: Some 250 lines of profusely commented and clear code.
2. Python: Currently the most commonly used language in science.
3. Modular structure: very easy to replace, modify and extend.
4. Easy and quick updates: The databases and results are updated with new ligands and proteins just by reruning the script.
5. Completely free and open source. 
6. Portability: Most dependencies can be installed via pip and/or conda.
7. No special hardware required.
8. Multithreading: the most time-consuming routines, Gypsum-DL and QVina-W, are parallelised.
9. Expandability: Nowadays there is Python code for ML-re-scoring, MD, QM, ADME-Tox prediction, etc.

MORE INFORMATION

https://sciting.eu/sciting/samples/covid-19-repurposing/

